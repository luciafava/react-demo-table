import React from "react";
import { DataTable } from "./components/DataTable/DataTable";
import './App.css';

const App = () => {
  return (
    <>
      <DataTable />
    </>
  );
}

export default App;