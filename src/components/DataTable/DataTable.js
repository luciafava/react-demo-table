import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import axios from 'axios';
import Table from "@material-ui/core/Table";
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Pagination } from '@material-ui/lab';
import { Button, Input, Switch } from 'antd';

// options for select
const options = [
  { value: 1, label: "1"},
  { value: 2, label: "2"},
  { value: 3, label: "3"},
  { value: 4, label: "4"},
  { value: 5, label: "5"}
]

export const DataTable = () => {
  // fetch Data
  const [gridData, setGridData] = useState([]);
  const [loading, setLoading] = useState([]);
   // Pagination
   const [page, setPage] = useState(1);
  // Search
  const [searchText, setSearchText] = useState("");
  let [filteredData] =  useState();

  // Toggle
  const [toggle, setToggle] = useState(false);
  const toggler = () => {
    toggle ? setToggle(false): setToggle(true);
  }

  useEffect(() => {
    loadData();
  }, [page]);

  const loadData = async () => {
    setLoading(true);
    const response = await axios.get(
      `https://jsonplaceholder.typicode.com/todos?_page=${page}`
    );
    setGridData(response.data);
    setLoading(false);
  };

  const modifiedData = gridData.map(({ title, ...item}) => ({
    ...item,
    key: item.id,
    title: title,
  }));

  // Handle Search
  const handleSearch = (e) => {
    setSearchText(e.target.value);
    if(e.target.value === "") {
      loadData();
    }
  };

  // Search
  const globalSearch = () => {
    filteredData = modifiedData.filter((value) => {
      return (
        value.title.toLowerCase().includes(searchText.toLowerCase())
      );
    });
    setGridData(filteredData);
  };

  // Reset
  const clearAll = () => {
    setSearchText("");
    loadData();
  }

  // Select
  const [Displayvalue, getvalue] = useState();
  const selectHandle = (e) => {
    //getvalue
    console.log(e);
  }
  return (
    <div className="wrapper form">
      <div className="wrapper__filter">
        <div className="filter__container">
        <h3 className="filter__title">Filters</h3>
        <form className="filter__form">
          <Button 
            className="cta" 
            variant="contained"
            onClick={globalSearch}>
              <i className="fas fa-search"></i>
          </Button>
          <Input
            placeholder="Search..."
            onChange={handleSearch}
            type="text"
            value={searchText}
          />
        </form>
        <div className="filter__completed">
          <h3 className="filter__completed-title">Completed</h3>
          {toggle ? <span className="toggle__style">YES</span>: <span className="toggle__style">NO</span>}
          <Switch onClick={toggler}/>
        </div>
        <div className="filter__select">
          <h3 className="filter__select-title">Select user id</h3>
          <Select 
            isMulti 
            options={options}
          >
          </Select>
        </div>
          <div className="reset__filters">
            <a onClick={clearAll} href="#">Reset Filters</a>
          </div>
        </div>
      </div>
      <div className="wrapper__table">
      <TableContainer>
      <Table 
        aria-label="simple table"
        loading={loading}
        dataSource={
          filteredData && filteredData.length ? filteredData : modifiedData
        }
      >
      <TableHead>
        <TableRow>
          <TableCell>UserId</TableCell>
          <TableCell>Title</TableCell>
          <TableCell>Completed</TableCell>
        </TableRow>
      </TableHead>
      <br />
      <TableBody>
        {gridData.map((grid) => (
          <TableRow key={grid.id}>
            <TableCell component="th" scope="row">
              {grid.userId}
            </TableCell>
            <TableCell className="ellipsis first"><span>{grid.title}</span></TableCell>
            <TableCell>
            <td className={`gridData ${grid.completed ? 
              'completed': 'nonCompleted'}`} 
              >
            </td>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
    <div className="pagination">
      <Pagination
        count={gridData.length}
        color="primary"
        variant="outlined"
        defaultPage={page}
        onChange={(e, value) => setPage(value)}
      />
    </div>
    </TableContainer>
    </div>
  </div>
  )
}